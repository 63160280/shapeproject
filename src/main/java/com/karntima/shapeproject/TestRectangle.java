/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeproject;

/**
 *
 * @author User
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3.0,2.0);
        System.out.println("Area of Rectangle (l = "+rectangle1.getL()+", w = "+rectangle1.getW()+") is " + rectangle1.calAreaR());
        rectangle1.setL(4);
        rectangle1.setW(5);
        System.out.println("Area of Rectangle (l = "+rectangle1.getL()+", w = "+rectangle1.getW()+") is " + rectangle1.calAreaR());
        rectangle1.setL(0);
        rectangle1.setW(0);
        System.out.println("Area of Rectangle (l = "+rectangle1.getL()+", w = "+rectangle1.getW()+") is " + rectangle1.calAreaR());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeproject;

/**
 *
 * @author User
 */
public class Square {
    private double s;
    
    public Square(double s){
        this.s = s;
    }
    public double calAreaS(){
        return s*s;
    }
    
    //get/set
    public double getS(){
        return s;
    }
    public void setS(double s){
        if(s<=0){
            System.out.println("Error: side must more than zero!!!");
            return ;
        }
        this.s = s;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeproject;

/**
 *
 * @author User
 */
public class Circle {
    private double r;
    public static final double pi = 22.0/7;
    public Circle(double r){
        this.r = r;
    }
    public double calArea(){
        return pi*r*r;
    }
    
    //Encapsulation
    public double getR(){
        return r;
    }
    public void setR(double r){
        if(r<=0){
            System.out.println("Error: Redius must more than zero!!!");
            return;
        }
        this.r = r;
    }
}

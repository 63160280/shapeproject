/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeproject;

/**
 *
 * @author User
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(4.0,3.0);
        System.out.println("Area of Triangle (b = "+triangle1.getB()+", h = "+triangle1.getH()+") is " + triangle1.calAreaT());
        triangle1.setB(5);
        triangle1.setH(4);
        System.out.println("Area of Triangle (b = "+triangle1.getB()+", h = "+triangle1.getH()+") is " + triangle1.calAreaT());
        triangle1.setB(0);
        triangle1.setH(0);
        System.out.println("Area of Triangle (b = "+triangle1.getB()+", h = "+triangle1.getH()+") is " + triangle1.calAreaT());
    }
}
